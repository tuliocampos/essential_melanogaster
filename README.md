# Combined use of feature engineering and machine-learning to predict essential genes in *Drosophila melanogaster* #


* To download this repository:

```
git clone https://www.bitbucket.org/tuliocampos/essential_melanogaster.git
```
- Requirements: R (systematic feature selection and machine-learning)

- Optional: R-Markdown, BASH and Python (scoring system and feature engineering)


## How to use? ##

This repository allows you to run the systematic ML approaches without manual intervention. All the required scripts and data are readily available.  

 **1.** Install the R libraries listed in the file "./Scripts/LIBRARIES".

 **2.** Because the lists of  genes provisionally annotated for essentiality ("./Genes\_and\_Sequences/\*.list") and their respective features ("./Features/Feature\_collection.txt") are readily available, the R scripts ("./Scripts/\*.R") encompass all the steps required for the ML analyses: load the feature data, label the genes, and run the systematic subsampling/feature selection/ML approaches. 

For example, you can run the systematic approach using the FULL, NR or NR\_SELECTED (or NR\_SELECTED - with bootstrapping) gene-feature sets using the following commands (nohup or screen are recommended to run in the background):

```

#Enter directory
cd Scripts

#Run ML with FULL data
Rscript FULL_script.R

#Run ML with NR data
Rscript NR_script.R

#Run ML with NR_SELECTED data (fastest)
Rscript NR_SELECTED_script.R

#Run bootstrap ML with NR_SELECTED
Rscript NR_SELECTED_1000bootstraps_script.R
```

These commands will load the required genes and features for each of the data sets, and run the sub-sampling and feature selection followed by ML approaches.

The results will be written into the current directory.

You can also run the scripts using Rstudio! 

Modify the scripts if you want to use more or less than 32 CPU cores.

 **3.** If you want to explore the feature data, load the collection into an R data.frame (columns represent features, rows represent genes):
 
```
x=read.delim("./Features/Feature_collection.txt",sep="\t",header=T,row.names=1)

#Shows the first feature names
head(colnames(x))

#Shows the first gene names
head(rownames(x))
``` 

Filters can be applied to select features and/or genes by their names. For example, selecting only the 25 most predictive features identified in the study:

```
library(dplyr)
best_pred=read.delim("./Features/25_best_predictors.txt",header=F)
x=x %>% dplyr::select(one_of(as.character(best_pred$V1)))
```

To select "essential" or "non-essential" gene features, load the "./Genes\_and\_Sequences/\*list.txt" files and use as filters.

For example, to select features of the (provisional) essential genes in the NR set:

```
essential=read.delim("./Genes_and_Sequences/Essential_NR_list.txt",header=F)
x=x[which(rownames(x) %in% as.character(essential$V1)),]
```

To convert the data.frame into a matrix format (if necessary, for ML inputs for example):

```
x_names=rownames(x)
x=sapply(x,as.numeric)
rownames(x)=x_names
```

 **4.** To apply the ML approach for other species (new data):

 - Create a collection of features from genes and save it in the same format as the Feature\_collection.txt file, with rows representing the genes, and columns representing the features.
 - Create two files containing the lists of essential or non-essential gene names, one name per line (see the \*.list files)
 - Modify the inputs in the FULL\_script.R, pointing to the new files
 - Run the R script as described in **2.**

---------------------------------------------------------------------

## Files and Directories ##

* Software and library versions: SoftwareVersions.txt and sessionInfo.txt
* Licensing: LICENCE

Genes and Sequences data:

* Nucleotide and protein sequences per gene: "\*.fasta" files
* Gene lists per essentiality annotation: "\*list.txt" files
* List of protein-coding genes: DM_protein_coding_genes.txt

Features:

* The file containing the features extracted for each gene (features with low variance were removed): "Feature\_collection.txt"
* Details about the collection: "Features-descriptions.xlsx" 
* The final set of 25 highly predictive features of gene essentiality: 25\_best\_predictors.txt

Scripts:

* Scripts used for the systematic feature selection and machine-learning approaches (FULL, NR and NR\_SELECTED data sets - includes the 1000 bootstraps evaluation): see the ".R" files
* R and shell scripts used to for the Essentiality Score (ES) and extraction of features: "Rmarkdown\_Features\_Plots.Rmd" (for reference only - not fully automated - refer to the #comments in this script or the Materials and Methods section in the manuscript for references to external data sets)

Data for plots (Data\_plots):

* Coordinate files for the assessment of genomic locations: "\*.coord" files
* Ordered essentiality scores for plotting: Essentiality\_scores.txt
* The number of SNPs identified by genomic location using the VCF file from FlyVar: "Frequencies\_SNPs.txt"  

## Licence ##

MIT

## Cite ##


If you use these scripts or data, please cite: 

**Combined use of feature engineering and machine-learning to predict essential genes in Drosophila melanogaster**

Tulio L Campos, Pasi K Korhonen, Andreas Hofmann, Robin B Gasser, Neil D Young

**NAR Genomics and Bioinformatics**, Volume 2, Issue 3, September 2020, lqaa051, https://doi.org/10.1093/nargab/lqaa051
